<?php
return [
    'label.id' => '#',
    'label.external_id' => 'Внешний id',
    'label.external_type' => 'Внешний тип',
    'label.field_id' => 'Поле анкеты',
    'label.value' => 'Значение',
    'label.status' => 'Статус',
    'label.created_at' => 'Дата создания',
    'label.updated_at' => 'Дата обновления',

    'hint.id' => "\r",
    'hint.external_id' => "\r",
    'hint.external_type' => "\r",
    'hint.field_id' => "\r",
    'hint.value' => "\r",
    'hint.status' => "\r",
    'hint.created_at' => "\r",
    'hint.updated_at' => "\r",

    'placeholder.id' => "\r",
    'placeholder.external_id' => "\r",
    'placeholder.external_type' => "\r",
    'placeholder.field_id' => "\r",
    'placeholder.value' => "\r",
    'placeholder.status' => "\r",
    'placeholder.created_at' => "\r",
    'placeholder.updated_at' => "\r",
];
