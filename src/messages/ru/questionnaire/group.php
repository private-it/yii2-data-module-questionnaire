<?php
return [
    'label.id' => '#',
    'label.name' => 'Системное имя',
    'label.label' => 'Название',
    'label.description' => 'Описание',
    'label.status' => 'Статус',
    'label.updated_at' => 'Дата обновления',
    'label.created_at' => 'Дата создания',

    'hint.id' => "\r",
    'hint.name' => "\r",
    'hint.label' => "\r",
    'hint.description' => "\r",
    'hint.status' => "\r",
    'hint.updated_at' => "\r",
    'hint.created_at' => "\r",

    'placeholder.id' => "\r",
    'placeholder.name' => "\r",
    'placeholder.label' => "\r",
    'placeholder.description' => "\r",
    'placeholder.status' => "\r",
    'placeholder.updated_at' => "\r",
    'placeholder.created_at' => "\r",
];
