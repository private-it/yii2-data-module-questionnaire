<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160608_124513_001_change_data
 *
 */
class m160608_124513_001_change_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Data::tableName(), 'external_type', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Data::tableName(), 'external_type');
    }
}