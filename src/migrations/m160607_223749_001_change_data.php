<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160607_223749_001_change_data
 *
 */
class m160607_223749_001_change_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Data::tableName(), 'status', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Data::tableName(), 'status');
    }
}