<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160607_121714_001_change_group
 *
 */
class m160607_121714_001_change_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Group::tableName(), 'status', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Group::tableName(), 'status');
    }
}