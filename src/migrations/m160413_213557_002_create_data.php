<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160413_213557_002_create_data
 *
 */
class m160413_213557_002_create_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(\PrivateIT\modules\questionnaire\models\Data::tableName(), [
            'id' => $this->primaryKey(),
            'external_id' => $this->integer()->defaultValue(0),
            'field_id' => $this->integer()->defaultValue(0),
            'value' => $this->text()->defaultValue(""),
            'created_at' => $this->timestamp()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(\PrivateIT\modules\questionnaire\models\Data::tableName());
    }
}