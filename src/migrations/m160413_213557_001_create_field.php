<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160413_213557_001_create_field
 *
 */
class m160413_213557_001_create_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(\PrivateIT\modules\questionnaire\models\Field::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue(""),
            'label' => $this->string()->defaultValue(""),
            'type' => $this->integer()->defaultValue(0),
            'params' => $this->text()->defaultValue(""),
            'status' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(\PrivateIT\modules\questionnaire\models\Field::tableName());
    }
}