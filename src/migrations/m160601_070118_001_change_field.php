<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160601_070118_001_change_field
 *
 */
class m160601_070118_001_change_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'group_id', $this->integer()->defaultValue(0));
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'sort', $this->integer()->defaultValue(0));
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'multiple', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'group_id');
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'sort');
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'multiple');
    }
}