<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160606_075700_002_change_group
 *
 */
class m160606_075700_002_change_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Group::tableName(), 'description', $this->text()->defaultValue(""));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Group::tableName(), 'description');
    }
}