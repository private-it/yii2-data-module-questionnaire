<?php

use PrivateIT\modules\questionnaire\models\Field;
use yii\db\Schema;
use yii\db\Migration;

/**
 * m190928_010101_001_add_field_required
 *
 */
class m190928_010101_001_add_field_required extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(Field::tableName(), 'required', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(Field::tableName(), 'required');
    }
}