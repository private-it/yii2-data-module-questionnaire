<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160606_075700_001_change_field
 *
 */
class m160606_075700_001_change_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'description', $this->text()->defaultValue(""));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropColumn(\PrivateIT\modules\questionnaire\models\Field::tableName(), 'description');
    }
}