<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\questionnaire\models;

use PrivateIT\modules\questionnaire\models\query\DataActiveQuery;
use PrivateIT\modules\questionnaire\QuestionnaireModule;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Field
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property string $label
 * @property string $description
 * @property integer $type
 * @property string $params
 * @property integer $sort
 * @property boolean $multiple
 * @property boolean $required
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Data[] $datas
 * @property Group $group
 */
class Field extends ActiveRecord
{
    const STATUS_ARCHIVED = -1;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_STRING = 1;
    const TYPE_NUMBER = 2;
    const TYPE_TEXT = 3;
    const TYPE_RADIO = 4;
    const TYPE_CHECK = 5;
    const TYPE_SELECT = 6;
    const TYPE_FILE = 7;

    /**
     * Get object statuses
     *
     * @return array
     */
    static function getStatuses()
    {
        return [
            static::STATUS_ARCHIVED => Yii::t('questionnaire/field', 'const.status.archived'),
            static::STATUS_DELETED => Yii::t('questionnaire/field', 'const.status.deleted'),
            static::STATUS_ACTIVE => Yii::t('questionnaire/field', 'const.status.active'),
        ];
    }

    /**
     * Convert values for saved
     *
     * @param $values
     * @return array
     */
    public static function convertValues($values)
    {
        $data = [];
        if (is_array($values) && isset($values[0]) && is_array($values[0])) {
            foreach ($values[0] as $index => $firstValue) {
                $itemIndex = 0;
                do {
                    $data[$index][$itemIndex] = $values[$itemIndex][$index];
                } while (isset($values[++$itemIndex], $values[$itemIndex][$index]));
            }
        }
        return $data;
    }

    /**
     * @inheritdoc
     * @return query\FieldActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return QuestionnaireModule::tableName(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('questionnaire/field', 'label.id'),
            'group_id' => Yii::t('questionnaire/field', 'label.group_id'),
            'name' => Yii::t('questionnaire/field', 'label.name'),
            'label' => Yii::t('questionnaire/field', 'label.label'),
            'description' => Yii::t('questionnaire/field', 'label.description'),
            'type' => Yii::t('questionnaire/field', 'label.type'),
            'params' => Yii::t('questionnaire/field', 'label.params'),
            'sort' => Yii::t('questionnaire/field', 'label.sort'),
            'multiple' => Yii::t('questionnaire/field', 'label.multiple'),
            'required' => Yii::t('questionnaire/field', 'label.required'),
            'status' => Yii::t('questionnaire/field', 'label.status'),
            'created_at' => Yii::t('questionnaire/field', 'label.created_at'),
            'updated_at' => Yii::t('questionnaire/field', 'label.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('questionnaire/field', 'hint.id'),
            'group_id' => Yii::t('questionnaire/field', 'hint.group_id'),
            'name' => Yii::t('questionnaire/field', 'hint.name'),
            'label' => Yii::t('questionnaire/field', 'hint.label'),
            'description' => Yii::t('questionnaire/field', 'hint.description'),
            'type' => Yii::t('questionnaire/field', 'hint.type'),
            'params' => Yii::t('questionnaire/field', 'hint.params'),
            'sort' => Yii::t('questionnaire/field', 'hint.sort'),
            'multiple' => Yii::t('questionnaire/field', 'hint.multiple'),
            'required' => Yii::t('questionnaire/field', 'hint.required'),
            'status' => Yii::t('questionnaire/field', 'hint.status'),
            'created_at' => Yii::t('questionnaire/field', 'hint.created_at'),
            'updated_at' => Yii::t('questionnaire/field', 'hint.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
            'id' => Yii::t('questionnaire/field', 'placeholder.id'),
            'group_id' => Yii::t('questionnaire/field', 'placeholder.group_id'),
            'name' => Yii::t('questionnaire/field', 'placeholder.name'),
            'label' => Yii::t('questionnaire/field', 'placeholder.label'),
            'description' => Yii::t('questionnaire/field', 'placeholder.description'),
            'type' => Yii::t('questionnaire/field', 'placeholder.type'),
            'params' => Yii::t('questionnaire/field', 'placeholder.params'),
            'sort' => Yii::t('questionnaire/field', 'placeholder.sort'),
            'multiple' => Yii::t('questionnaire/field', 'placeholder.multiple'),
            'required' => Yii::t('questionnaire/field', 'placeholder.required'),
            'status' => Yii::t('questionnaire/field', 'placeholder.status'),
            'created_at' => Yii::t('questionnaire/field', 'placeholder.created_at'),
            'updated_at' => Yii::t('questionnaire/field', 'placeholder.updated_at'),
        ];
    }

    /**
     * Get value from Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to Id
     *
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get value from GroupId
     *
     * @return string
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Set value to GroupId
     *
     * @param $value
     * @return $this
     */
    public function setGroupId($value)
    {
        $this->group_id = $value;
        return $this;
    }

    /**
     * Get value from Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value to Name
     *
     * @param $value
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Get value from Label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set value to Label
     *
     * @param $value
     * @return $this
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }

    /**
     * Get value from Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set value to Description
     *
     * @param $value
     * @return $this
     */
    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    /**
     * Get value from Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value to Type
     *
     * @param $value
     * @return $this
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }

    /**
     * Get value from Params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set value to Params
     *
     * @param $value
     * @return $this
     */
    public function setParams($value)
    {
        $this->params = $value;
        return $this;
    }

    /**
     * Get value from Sort
     *
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set value to Sort
     *
     * @param $value
     * @return $this
     */
    public function setSort($value)
    {
        $this->sort = $value;
        return $this;
    }

    /**
     * Get value from Multiple
     *
     * @return string
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Set value to Multiple
     *
     * @param $value
     * @return $this
     */
    public function setMultiple($value)
    {
        $this->multiple = $value;
        return $this;
    }

    /**
     * Get value from required
     *
     * @return string
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set value to required
     *
     * @param $value
     * @return $this
     */
    public function setRequired($value)
    {
        $this->required = $value;
        return $this;
    }

    /**
     * Get value from Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set value to Status
     *
     * @param $value
     * @return $this
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    /**
     * Get value from CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set value to CreatedAt
     *
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
        return $this;
    }

    /**
     * Get value from UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set value to UpdatedAt
     *
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
        return $this;
    }

    /**
     * Get relation Data[]
     *
     * @param string $class
     * @return query\DataActiveQuery
     */
    public function getDatas($class = '\Data')
    {
        return $this->hasMany(static::findClass($class, __NAMESPACE__), ['field_id' => 'id']);
    }

    /**
     * Get relation Group
     *
     * @param string $class
     * @return query\GroupActiveQuery
     */
    public function getGroup($class = '\Group')
    {
        return $this->hasOne(static::findClass($class, __NAMESPACE__), ['id' => 'group_id']);
    }

    /**
     * Get field data object
     *
     * @param $externalId
     * @param int $externalType
     * @return DataActiveQuery
     */
    public function getData($externalId, $externalType = 0)
    {
        return $this->getDatas()->andWhere([
            'field_id' => $this->id,
            'external_id' => $externalId,
            'external_type' => $externalType
        ]);
    }

    /**
     * Add field value
     *
     * @param $value
     * @param $externalId
     * @param int $externalType
     * @return array|null|Data
     * @throws \yii\base\InvalidConfigException
     */
    public function writeData($value, $externalId, $externalType = 0)
    {
        $rec = $this->getData($externalId, $externalType)->one();
        if (null === $rec) {
            /** @var Data $rec */
            $rec = Yii::createObject(static::findClass('\Data', __NAMESPACE__));
            $rec->setFieldId($this->id);
            $rec->setExternalId($externalId);
            $rec->setExternalType($externalType);
        }
        $rec->setStatus($rec::STATUS_ACTIVE);
        $rec->setValue(json_encode(self::convertValues($value)));
        $rec->save(false);
        return $rec;
    }

    /**
     * Read field value
     *
     * @param $externalId
     * @param int $externalType
     * @return bool|mixed
     */
    public function readData($externalId, $externalType = 0)
    {
        $rec = $this->getData($externalId, $externalType)->andWhere(['=', 'status', static::STATUS_ACTIVE])->one();
        if (null !== $rec) {
            return json_decode($rec->getValue(), true);
        }
        return false;
    }
}
