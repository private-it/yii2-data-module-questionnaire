<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\questionnaire\models;

use PrivateIT\modules\questionnaire\QuestionnaireModule;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Group
 *
 * @property integer $id
 * @property string $name
 * @property string $label
 * @property string $description
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Field[] $fields
 */
class Group extends ActiveRecord
{
    const STATUS_ARCHIVED = -1;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Get object statuses
     *
     * @return array
     */
    static function getStatuses()
    {
        return [
            static::STATUS_ARCHIVED => Yii::t('questionnaire/group', 'const.status.archived'),
            static::STATUS_DELETED => Yii::t('questionnaire/group', 'const.status.deleted'),
            static::STATUS_ACTIVE => Yii::t('questionnaire/group', 'const.status.active'),
        ];
    }

    /**
     * @inheritdoc
     * @return query\GroupActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return QuestionnaireModule::tableName(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('questionnaire/group', 'label.id'),
            'name' => Yii::t('questionnaire/group', 'label.name'),
            'label' => Yii::t('questionnaire/group', 'label.label'),
            'description' => Yii::t('questionnaire/group', 'label.description'),
            'status' => Yii::t('questionnaire/group', 'label.status'),
            'updated_at' => Yii::t('questionnaire/group', 'label.updated_at'),
            'created_at' => Yii::t('questionnaire/group', 'label.created_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('questionnaire/group', 'hint.id'),
            'name' => Yii::t('questionnaire/group', 'hint.name'),
            'label' => Yii::t('questionnaire/group', 'hint.label'),
            'description' => Yii::t('questionnaire/group', 'hint.description'),
            'status' => Yii::t('questionnaire/group', 'hint.status'),
            'updated_at' => Yii::t('questionnaire/group', 'hint.updated_at'),
            'created_at' => Yii::t('questionnaire/group', 'hint.created_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
            'id' => Yii::t('questionnaire/group', 'placeholder.id'),
            'name' => Yii::t('questionnaire/group', 'placeholder.name'),
            'label' => Yii::t('questionnaire/group', 'placeholder.label'),
            'description' => Yii::t('questionnaire/group', 'placeholder.description'),
            'status' => Yii::t('questionnaire/group', 'placeholder.status'),
            'updated_at' => Yii::t('questionnaire/group', 'placeholder.updated_at'),
            'created_at' => Yii::t('questionnaire/group', 'placeholder.created_at'),
        ];
    }

    /**
     * Get value from Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to Id
     *
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get value from Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value to Name
     *
     * @param $value
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Get value from Label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set value to Label
     *
     * @param $value
     * @return $this
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }

    /**
     * Get value from Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set value to Description
     *
     * @param $value
     * @return $this
     */
    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    /**
     * Get value from Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set value to Status
     *
     * @param $value
     * @return $this
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    /**
     * Get value from UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set value to UpdatedAt
     *
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
        return $this;
    }

    /**
     * Get value from CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set value to CreatedAt
     *
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
        return $this;
    }

    /**
     * Get relation Field[]
     *
     * @param string $class
     * @return query\FieldActiveQuery
     */
    public function getFields($class = '\Field')
    {
        return $this->hasMany(static::findClass($class, __NAMESPACE__), ['group_id' => 'id']);
    }

}
