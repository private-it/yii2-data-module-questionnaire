<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\questionnaire\models\query;

use PrivateIT\modules\questionnaire\models\Field;

/**
 * FieldActiveQuery
 *
 */
class FieldActiveQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Field[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Field|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */
}
