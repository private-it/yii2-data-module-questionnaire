<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\questionnaire\models;

use PrivateIT\modules\questionnaire\QuestionnaireModule;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Data
 *
 * @property integer $id
 * @property integer $external_id
 * @property integer $external_type
 * @property integer $field_id
 * @property string $value
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Field $field
 */
class Data extends ActiveRecord
{
    const STATUS_ARCHIVED = -1;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Get object statuses
     *
     * @return array
     */
    static function getStatuses()
    {
        return [
            static::STATUS_ARCHIVED => Yii::t('questionnaire/data', 'const.status.archived'),
            static::STATUS_DELETED => Yii::t('questionnaire/data', 'const.status.deleted'),
            static::STATUS_ACTIVE => Yii::t('questionnaire/data', 'const.status.active'),
        ];
    }

    /**
     * @inheritdoc
     * @return query\DataActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return QuestionnaireModule::tableName(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('questionnaire/data', 'label.id'),
            'external_id' => Yii::t('questionnaire/data', 'label.external_id'),
            'external_type' => Yii::t('questionnaire/data', 'label.external_type'),
            'field_id' => Yii::t('questionnaire/data', 'label.field_id'),
            'value' => Yii::t('questionnaire/data', 'label.value'),
            'status' => Yii::t('questionnaire/data', 'label.status'),
            'created_at' => Yii::t('questionnaire/data', 'label.created_at'),
            'updated_at' => Yii::t('questionnaire/data', 'label.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('questionnaire/data', 'hint.id'),
            'external_id' => Yii::t('questionnaire/data', 'hint.external_id'),
            'external_type' => Yii::t('questionnaire/data', 'hint.external_type'),
            'field_id' => Yii::t('questionnaire/data', 'hint.field_id'),
            'value' => Yii::t('questionnaire/data', 'hint.value'),
            'status' => Yii::t('questionnaire/data', 'hint.status'),
            'created_at' => Yii::t('questionnaire/data', 'hint.created_at'),
            'updated_at' => Yii::t('questionnaire/data', 'hint.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
            'id' => Yii::t('questionnaire/data', 'placeholder.id'),
            'external_id' => Yii::t('questionnaire/data', 'placeholder.external_id'),
            'external_type' => Yii::t('questionnaire/data', 'placeholder.external_type'),
            'field_id' => Yii::t('questionnaire/data', 'placeholder.field_id'),
            'value' => Yii::t('questionnaire/data', 'placeholder.value'),
            'status' => Yii::t('questionnaire/data', 'placeholder.status'),
            'created_at' => Yii::t('questionnaire/data', 'placeholder.created_at'),
            'updated_at' => Yii::t('questionnaire/data', 'placeholder.updated_at'),
        ];
    }

    /**
     * Get value from Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to Id
     *
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get value from ExternalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->external_id;
    }

    /**
     * Set value to ExternalId
     *
     * @param $value
     * @return $this
     */
    public function setExternalId($value)
    {
        $this->external_id = $value;
        return $this;
    }

    /**
     * Get value from ExternalType
     *
     * @return string
     */
    public function getExternalType()
    {
        return $this->external_type;
    }

    /**
     * Set value to ExternalType
     *
     * @param $value
     * @return $this
     */
    public function setExternalType($value)
    {
        $this->external_type = $value;
        return $this;
    }

    /**
     * Get value from FieldId
     *
     * @return string
     */
    public function getFieldId()
    {
        return $this->field_id;
    }

    /**
     * Set value to FieldId
     *
     * @param $value
     * @return $this
     */
    public function setFieldId($value)
    {
        $this->field_id = $value;
        return $this;
    }

    /**
     * Get value from Value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value to Value
     *
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value from Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set value to Status
     *
     * @param $value
     * @return $this
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    /**
     * Get value from CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set value to CreatedAt
     *
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
        return $this;
    }

    /**
     * Get value from UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set value to UpdatedAt
     *
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
        return $this;
    }

    /**
     * Get relation Field
     *
     * @param string $class
     * @return query\FieldActiveQuery
     */
    public function getField($class = '\Field')
    {
        return $this->hasOne(static::findClass($class, __NAMESPACE__), ['id' => 'field_id']);
    }

}
